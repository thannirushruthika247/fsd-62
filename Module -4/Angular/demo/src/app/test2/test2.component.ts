import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component implements OnInit {

  person: any;

  constructor() {
    this.person = {
      id: 101,
      name: 'Harsha',
      age: 25,
      hobbies: ['Running', 'Music', 'Movies', 'GYM', 'Reading'],
      address: {streetNo: 111, city: 'Hyd', state: 'Telangana'}
    };
  }
  
  ngOnInit() {
  }
  submit() {
    alert("Id:" + this.person.id + "\n" + "Name: " + this.person.name);
    console.log(this.person);
  }

}