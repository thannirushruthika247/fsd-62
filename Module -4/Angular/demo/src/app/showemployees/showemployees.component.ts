import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit{
employees : any;
emailId: any;
constructor(private service: EmpService) {
  this.emailId = localStorage.getItem('emailId');
  // this.employees = [
  //     {empId:101, empName:'Shruthika', salary:121212.12, gender:'Female',   country:'IND', doj:'06-13-2018'},
  //     {empId:102, empName:'Vaishu',    salary:121212.23, gender:'Female',   country:'IND', doj:'07-14-2017'},
  //     {empId:103, empName:'Manasa',     salary:121212.34, gender:'Female', country:'IND', doj:'08-15-2016'},
  //     {empId:104, empName:'Sahithya',  salary:121212.45, gender:'Female', country:'IND', doj:'09-16-2015'},
  //     {empId:105, empName:'Deekshi',   salary:121212.56, gender:'Female',   country:'IND', doj:'10-17-2014'}
     
  // ];
    
  }
  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any)=> {
      console.log(data);
      this.employees=data;
    })
  }
  deleteEmployee(emp: any) {
    this.service.deleteEmployeeById(emp.empId).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.employees.findIndex((employee: any) => {
      return emp.empId == employee.empId;
    });

    this.employees.splice(i, 1);
}

}

