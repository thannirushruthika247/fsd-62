// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-showempbyid',
//   templateUrl: './showempbyid.component.html',
//   styleUrl: './showempbyid.component.css'
// })
// export class ShowempbyidComponent {

//   employees: any; 
//   emp: any;
//   msg: string;
//   emailId: any;

//   //Date Format: mm-DD-YYYY
//   constructor() {

//     this.msg = "";
//     this.emailId = localStorage.getItem('emailId');

//     this.employees = [
//       {empId:101, empName:'Shruthika',  salary:1212.12, gender:'Female',   country:'IND', doj:'06-13-2018'},
//       {empId:102, empName:'Vaishu',     salary:2323.23, gender:'Female',   country:'IND', doj:'07-14-2017'},
//       {empId:103, empName:'Manasa',     salary:3434.34, gender:'Female',   country:'IND', doj:'08-15-2016'},
//       {empId:104, empName:'Sahithya',   salary:4545.45, gender:'Female',   country:'IND', doj:'09-16-2015'},
//       {empId:105, empName:'Deekshitha', salary:5656.56, gender:'Female',   country:'IND', doj:'10-17-2014'}
//     ];
//   }
  
//   getEmployee(employee: any) {

//     this.emp = null;
   
//     this.employees.forEach((element: any) => {
//       if (element.empId == employee.empId) {
//         this.emp = element;
//       }
//     });
    
//     this.msg = "Employee Record Not Found!!!";
    
//   }


// }





import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent {

  emp: any;
  msg: string;
  emailId: any;

  constructor(private service: EmpService) {
    this.msg = "";
    this.emailId = localStorage.getItem('emailId');
  }

  async getEmployee(employee: any) {
    this.msg = "";
    this.emp = null;
   
    await this.service.getEmployeeById(employee.empId).then((data: any) => {
      console.log(data);
      this.emp = data;
    });
   
    if (this.emp == null) {
      this.msg = "Employee Record Not Found!!!";
    }   
  }
}




