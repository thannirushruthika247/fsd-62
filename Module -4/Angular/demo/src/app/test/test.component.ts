import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {
  id: number;
  name: string;
  age: number;

  hobbies: any;
  address: any;

     constructor() {
      // alert("constructor invoked.........")
       this.id = 101;
       this.name = 'Shruthika';
       this.age = 23;


       this.hobbies = ['Running','Swimming','Dancing'];
       this.address = {streetNo: 21, city: 'Hyd', state: 'Telangana'};
       
     }
     ngOnInit() {
      // alert("ngOnInit Invoked............")
     }
}
