package com.ts.Student;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.EmailSenderService;



@EnableJpaRepositories(basePackages="com.dao")
@EntityScan(basePackages="com.model")
@SpringBootApplication(scanBasePackages="com")
public class StudentApplication {
	
//	@Autowired
//	private EmailSenderService senderService;

	public static void main(String[] args) {
		SpringApplication.run(StudentApplication.class, args);
	}
//   @EventListener(ApplicationReadyEvent.class)
//   
   public void sendMail() {
	   
//	senderService.sendEmail(toEmail: "thannirushruthika247@gmail.com", subject: "This is subject" , body: "This is body of Email");
  }
}