package com.dao;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Course;

@Service
public class CourseDao {
	@Autowired
	CourseRepository courseRepo;

	public List<Course> getAllCourses() {
		return courseRepo.findAll();
	}
	
	public Course getCourseById(int deptId) {
		return courseRepo.findById(deptId).orElse(null);
	}

	public Course addCourse(Course dept) {
		return courseRepo.save(dept);
	}

	public Course updateCourse(Course dept) {
		return courseRepo.save(dept);
	}

	public void deleteCourseById(int deptId) {
		courseRepo.deleteById(deptId);
	}
}
