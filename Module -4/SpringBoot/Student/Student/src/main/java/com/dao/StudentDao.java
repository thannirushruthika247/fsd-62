package com.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {

	@Autowired
	StudentRepository stdRepo;

	public Student stdLogin(String emailId, String password) {
		return stdRepo.stdLogin(emailId, password);
	}

	public List<Student> getAllStudents() {
		return stdRepo.findAll();
	}

	public Student getStudentById(int stdId) {
		return stdRepo.findById(stdId).orElse(null);
	}

	public List<Student> getStudentByName(String stdName) {
		return stdRepo.findByName(stdName);
	}

	public Student addStudent(Student std) {
		return stdRepo.save(std);
	}

	public Student updateStudent(Student std) {
		return stdRepo.save(std);
	}

	public void deleteStudentById(int stdId) {
		stdRepo.deleteById(stdId);
	}
	
	@PersistenceContext
    private EntityManager entityManager;

	public Student getStudentByEmail(String emailId) {
        String jpql = "SELECT s FROM Student s WHERE s.emailId = :emailId";
        TypedQuery<Student> query = entityManager.createQuery(jpql, Student.class);
        query.setParameter("emailId", emailId);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}