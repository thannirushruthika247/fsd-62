package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDao {

	@Autowired // Implementing Dependency Injection
	ProductRepository productRepository;

	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	public List<Product> getProductByName(String prodName) {
		return productRepository.findByName(prodName);
	}

	public Product addProduct(Product product) {
		return productRepository.save(product);
	}

	public Product updateProduct(Product product) {
		return productRepository.save(product);
	}

	public Product deleteProduct(Product product) {
		return productRepository.save(product);
	}

	public void deleteProductById(int productId) {
		productRepository.deleteById(productId);

	}
}
