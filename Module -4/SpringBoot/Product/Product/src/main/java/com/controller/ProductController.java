package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDao;
import com.model.Product;

@RestController
public class ProductController {

	@Autowired // Implementing Dependency Injection
	ProductDao productDao;

	@GetMapping("getAllProducts")
	public List<Product> getAllProducts() {
		return productDao.getAllProducts();
	}

	@GetMapping("getProductByName/{pname}")
	public List<Product> getProductByName(@PathVariable("pname") String prodName) {
		return productDao.getProductByName(prodName);
	}

	@PostMapping("addProduct")
	public Product addProduct(@RequestBody Product product) {
		return productDao.addProduct(product);
	}

	@PutMapping("updateProduct")
	public Product updateProduct(@RequestBody Product product) {
		return productDao.updateProduct(product);
	}

	@DeleteMapping("deleteProductBy/{id}")
	public String deleteProductById(@PathVariable("id") int productId) {
		productDao.deleteProductById(productId);
		return "Product with ProductId: " + productId + ", Deleted Successfully !!!";
	}

}
