package com.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/DeleteStudent")
public class DeleteStudent extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		
		StudentDao stdDao = new StudentDao();
		int result = stdDao.deleteStudent(studentId);
				
		if (result > 0) {
			request.getRequestDispatcher("GetAllStudents").include(request, response);
		} else {
			request.getRequestDispatcher("AdminHomePage").include(request, response);
			out.println("<h3 style='color:red;'>Failed to Delete the Student Record!!!</h3>");
		}
		out.println("</center>");
		

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
