package com.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/GetAllStudents")
public class GetAllStudents extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		StudentDao stdDao = new StudentDao();
		List<Student> stdList = stdDao.getAllStudents();
		
		if (stdList != null) {			
			request.setAttribute("stdList", stdList);			
			request.getRequestDispatcher("GetAllStudents.jsp").forward(request, response);	
		} else {
			request.getRequestDispatcher("AdminHomePage.jsp").include(request, response);
			out.println("<h3 style='color:red; align='center'>Unable to Fetch Student Records!!</h3>");
		}
	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
