package com.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/GetAllStudentById")
public class GetAllStudentById extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		
		StudentDao stdDao = new StudentDao();
		Student std = stdDao.getStudentById(studentId);
					
		if (std != null) {
			
			//Storing the employee data under request object
			request.setAttribute("std", std);

			RequestDispatcher rd = request.getRequestDispatcher("GetStudentById.jsp");
			rd.forward(request, response);

		} else {
			RequestDispatcher rd = request.getRequestDispatcher("AdminHomePage.jsp");
			rd.include(request, response);
			
			out.println("<h3 style='color:red;' align='center'>Student Record Not Found!!!</h3>");
		}		
		
		out.println("</center></body>");


	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
