package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/HRHomePage")
public class HRHomePage extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		String userName = request.getParameter("userName");
		
		out.println("<html>");
		out.println("<body bgcolor='lightyellow'>");
		out.println("<h3 style='color:green'>Welcome: " +userName+"!</h3>");
		out.println("<center>");
		out.println("<h1 style='color:green'>Welcome to HR Homepage </h1>");
		out.println("</center></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
