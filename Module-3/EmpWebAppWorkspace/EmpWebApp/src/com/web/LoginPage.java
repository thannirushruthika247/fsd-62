package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/LoginPage")
public class LoginPage extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");

		PrintWriter out = response.getWriter();

		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		if (userName.equals("HR") && password.equals("HR")) {
			RequestDispatcher rd=request.getRequestDispatcher("HRHomePage");
			rd.forward(request, response);
		}	
//			out.println("<h2 style='color:green'>Welcome to HR Page<h1>");
//		} else if (userName.equals(password) && !userName.equals("HR")) {
//			out.println("<h2 style='color:green'>Welcome to Employee page<h2>");
		 
			else {
			out.println("<h2 style='color:red'>Invalid Password<h2>");
			RequestDispatcher rd=request.getRequestDispatcher("login.html");
			rd.include(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
